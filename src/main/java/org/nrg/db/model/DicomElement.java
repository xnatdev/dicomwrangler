package org.nrg.db.model;

/**
 * Created by drm on 10/3/16.
 */
public class DicomElement {
    private String tagPathString;
    private String value;
    private String name;
    private int[] tags;
    private int occuranceCount;  // number of times this elements occurs.

    public DicomElement(String tagPathString, String name, String value, int occuranceCount) {
        this.tagPathString = tagPathString;
        this.name = name;
        this.value = value;
        this.tags = tagPathToArray( tagPathString);
        this.occuranceCount = occuranceCount;
    }

    public DicomElement(String tagPathString, String name, String value) {
        this( tagPathString, name, value, 1);
    }

    public DicomElement( int[] tags, String name, String value, int occuranceCount) {
        this.tagPathString = tagArrayToTagPath(tags);
        this.name = name;
        this.value = value;
        this.tags = tags;
        this.occuranceCount = occuranceCount;
    }

    public DicomElement( int[] tags, String name, String value) {
        this( tags, name, value, 1);
    }

    public String getTagPathString() {
        return tagPathString;
    }
    public void setTagPathString(String tagPathString) {
        this.tagPathString = tagPathString;
    }

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int[] getTags() {
        return tags;
    }

    public int getOccuranceCount() {
        return occuranceCount;
    }

    public void setOccuranceCount(int occuranceCount) {
        this.occuranceCount = occuranceCount;
    }

    public String toString() {
        return tagPathString + " " + value;
    }

    /**
     * Convert tag int array into tagPathString.
     * The tagPathString is assumed to have the form
     * "tag [[,item#,tag]...]". For example, "00181220,0,00100010".
     * @param tags
     * @return
     */
    public static String tagArrayToTagPath( int[] tags) {
        StringBuilder sb = new StringBuilder();
        for( int i = 0; i < tags.length; i++) {
            if( i % 2 == 0) {
                String s = String.format("%08X",tags[i]);
                sb.append(s).append(",");
            }
            else {
                sb.append( Integer.toString(tags[i])).append(",");
            }
        }
        sb.deleteCharAt(sb.length()-1);
        return sb.toString();
    }

    /**
     * Return int[] of tags from tagString with format ggggeeee [[,n,ggggeeee]...]
     * @param tagPathString
     * @return
     * @throws NumberFormatException if string does not contain parsable int.
     */
    private int[] tagPathToArray( String tagPathString) {
        String[] tokens = tagPathString.split(",");
        int[] tagArray = new int[tokens.length];
        for( int i = 0; i < tokens.length; i++) {
            if( i % 2 == 0)
                tagArray[i] = Integer.parseInt(tokens[i], 16);
            else
                tagArray[i] = Integer.parseInt(tokens[i], 10);
        }
        return tagArray;
    }
}

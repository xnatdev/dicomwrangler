package org.nrg.db.model;

import org.dcm4che2.io.DicomInputStream;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by drm on 10/3/16.
 */
public class DicomObject {
    private org.dcm4che2.data.DicomObject d4obj;
    private String parentTagPath;

    public DicomObject( File file) throws IOException {
        DicomInputStream dis = new DicomInputStream( file);
        d4obj = dis.readDicomObject();
        parentTagPath = "";
    }

    private DicomObject(org.dcm4che2.data.DicomObject d4obj, String parentTagPath) {
        this.d4obj = d4obj;
        this.parentTagPath = parentTagPath;
    }

    public List<DicomElement> getElements() {
        List<DicomElement> elements = new ArrayList<>();
        for( Iterator<org.dcm4che2.data.DicomElement> it = d4obj.iterator(0,0x7FE00009); it.hasNext();) {
            org.dcm4che2.data.DicomElement de = it.next();

            //System.out.println(parentTagPath + de);
            DicomElement dicomElement ;
            if( de.hasItems()) {
//                dicomElement = new DicomElement( parentTagPath + formatedTag(de.tag()), Tags.getName(de.tag()), null);
                dicomElement = new DicomElement( parentTagPath + formatedTag(de.tag()), "name", null);
                for( int i = 0; i < de.countItems(); i++) {
                    String tagPath = formatedTag(de.tag()) + "," + i + ",";
                    DicomObject childDicomObject = new DicomObject( de.getDicomObject(i), parentTagPath + tagPath);
                    elements.addAll( childDicomObject.getElements());
                }
            }
            else {
//                dicomElement = new DicomElement( parentTagPath + formatedTag(de.tag()), Tags.getName(de.tag()), d4obj.getString(de.tag()));
                dicomElement = new DicomElement( parentTagPath + formatedTag(de.tag()), "name", getTagValue(de.tag()));
//                String s = d4obj.getString(de.tag());
//                String[] ss = d4obj.getStrings(de.tag());
//                dicomElement = new DicomElement( parentTagPath + formatedTag(de.tag()), "name", d4obj.getString(de.tag()));
            }
            elements.add(dicomElement);
        }
        return elements;
    }

    /**
     * Read the specified tag value as a String.
     *
     * getStrings() is needed to get multiple values when the multiplicity is > 1 but throws UnsupportedOperationException
     * if the the VR is a binary type, in which case getString() is needed. This implementation is reactionary.  Always call
     * getStrings() and catch the exception and call getString() if needed.
     *
     * TODO: It might be better to be proactive and filter tags by VR and only call the appropriate version.
     *
     * @param tag
     * @return value of the tag as a String.
     */
    private String getTagValue( int tag) {
        String s;
        try {
            String[] strings = d4obj.getStrings(tag);
            if( strings.length == 1) {
                s = strings[0];
            }
            else {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < strings.length; i++) {
                    sb.append(strings[i]);
                    if (i < strings.length - 1) sb.append("\\");
                }
                s = sb.toString();
            }
        }
        catch (Exception e) {
            s = d4obj.getString( tag);
        }
        return s;
    }

    public String getStringValue( int[] tags) {
        return d4obj.getString( tags);
    }

    public static void main(String[] args) {
        DicomObject dicomObject = null;
        try {
            dicomObject = new DicomObject( new File("/home/drm/projects/data/dicom/MR/000000.dcm"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<DicomElement> dicomElements = dicomObject.getElements();

        for( DicomElement element: dicomElements) {
            System.out.println(element);
        }
    }

    private String formatedTag( int tag) {
        //        return String.format("(%s,%s)", s.substring(0,4), s.substring(4,8));
        return String.format("%08X", tag);
    }
}

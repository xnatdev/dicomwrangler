package org.nrg.db;

import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

//        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("YetAnotherDicomBrowser.fxml"));
//        primaryStage.setTitle("Yet Another DICOM Browser");
//        primaryStage.setScene(new Scene(root, 1200, 600));
//        primaryStage.show();

        ScreensConfiguration screens = context.getBean(ScreensConfiguration.class);
        screens.setPrimaryStage(primaryStage);
        screens.showMainScreen();

   }

    public static void main(String[] args) {
        launch(args);
    }
}

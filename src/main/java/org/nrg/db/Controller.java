package org.nrg.db;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;

import javax.swing.JFileChooser;
import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private TreeView<String> fileTreeView;
    @FXML
    private AnchorPane ap;

    private DirectoryChooser fileChooser;

//    private final Node rootIcon = new ImageView(
//            new Image(getClass().getResourceAsStream("folder_16.png"))
//    );


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.fileChooser = new DirectoryChooser();
        this.fileChooser.setTitle("Select file or directory to scan.");
//        TreeItem<String> rootItem = new TreeItem<> ("Inbox", rootIcon);
//        TreeItem<String> rootItem = new TreeItem<> ("Inbox");
//        rootItem.setExpanded(true);
//        for (int i = 1; i < 6; i++) {
//            TreeItem<String> item = new TreeItem<> ("Message" + i);
//            rootItem.getChildren().add(item);
//        }
//        fileTreeView.setRoot(rootItem);

    }

    @FXML
    private void handleOpenMenuAction(ActionEvent event) {
        // Button was clicked, do something...
//        outputTextArea.appendText("Button Action\n");
        System.out.println("Open menu item action.");
//        Stage stage = (Stage) ap.getScene().getWindow();
//        fileChooser.showDialog(stage);
        JFileChooser chooser = new JFileChooser(".");
        chooser.setMultiSelectionEnabled(true);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int ret = chooser.showOpenDialog(null);
        if(ret == JFileChooser.APPROVE_OPTION) {
            File[] files = chooser.getSelectedFiles();
            List<Path> rootPaths = new ArrayList<>(files.length);
            for( File f: files) {
                rootPaths.add(f.toPath());
            }

            List<File> dcmFiles = findFiles( files, null);

            fileTreeView.setRoot( getTreeView( dcmFiles));
        }
    }

    private TreeItem<String> getTreeView(List<File> dcmFiles) {
        TreeItem<String> rootItem = new TreeItem<> ("DICOM Files");
        rootItem.setExpanded(true);
        for (File f: dcmFiles) {
            TreeItem<String> item = new TreeItem<> (f.toString());
            rootItem.getChildren().add(item);
        }
        return rootItem;
    }

    private List<File> findFiles(File[] srcFiles, FileFilter fileFilter) {
        List<File> files = new ArrayList<>();
        for( File f: srcFiles) {
            if( f.isDirectory()) {
                files.addAll(findFiles(f.listFiles(), fileFilter));
            }
            else {
                if( fileFilter != null) {
                    if( fileFilter.matches(f)) {
                        files.add(f);
                    }
                }
                else {
                    files.add(f);
                }
            }
        }
        return files;
    }

//    private List<ResourcePath> findFiles(List<ResourcePath> rps, FileFilter fileFilter) {
//        List<ResourcePath> files = new ArrayList<>();
//        for( ResourcePath rp: rps) {
//            if (rp.isDirectory()) {
//                files.addAll(findFiles(rp.listFiles(), fileFilter));
//            } else {
//                if (fileFilter != null) {
//                    if (fileFilter.matches(f)) {
//                        files.add(f);
//                    }
//                } else {
//                    files.add(f);
//                }
//            }
//        }
//        return files;
//    }

    public class FileFilter {
        public boolean matches( File f) {
            return true;
        }
    }

}

package org.nrg.db;

import javafx.scene.control.TreeItem;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Created by drm on 9/23/16.
 */
public class DefaultTreeView {
    private TreeItem<String> rootItem;

    public DefaultTreeView(Map<Path, List<Path>> dcmFileMap) {
        rootItem = new TreeItem<>("DICOM Files");
        rootItem.setExpanded(true);
        for(Path p: dcmFileMap.keySet()) {
//            TreeItem<String> item = new TreeItem<> (p.toString());
            TreeItem<String> item = new FilePathTreeItem (p);
            item.setValue( p.toString());
            rootItem.getChildren().add(item);
            for( Path pp: dcmFileMap.get(p)) {
//                item.getChildren().add( new TreeItem( pp.toString()));
                item.getChildren().add( new FilePathTreeItem( pp));
            }
        }
    }

    public TreeItem<String> getRootItem() {
        return rootItem;
    }
}

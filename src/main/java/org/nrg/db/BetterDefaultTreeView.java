package org.nrg.db;

import javafx.scene.control.TreeItem;
import org.nrg.db.model.FileResource;
import org.nrg.db.ui.FileResourceTreeItem;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by drm on 9/23/16.
 */
public class BetterDefaultTreeView {
    private TreeItem<FileResourceTreeItem> rootItem;

    public BetterDefaultTreeView( List<FileResource> resources) {
        rootItem = new TreeItem<>(new FileResourceTreeItem("DICOM Files", null));
        rootItem.setExpanded(true);

        for(FileResource resource: resources) {
            TreeItem<FileResourceTreeItem> basePathItem = getBasePathItem( rootItem, resource.getBasePath());
            addChildren( basePathItem, resource.getRelativePath(), resource.getId());
        }
    }

    private TreeItem<FileResourceTreeItem> getBasePathItem( TreeItem<FileResourceTreeItem> item, Path basePath) {
        for( TreeItem<FileResourceTreeItem> ic: item.getChildren()) {
            if( ic.getValue().getDisplayName().equals( basePath.toString())) {
                return ic;
            }
        }
        TreeItem<FileResourceTreeItem> newItem = new TreeItem<>(new FileResourceTreeItem( basePath.toString(), null));
        item.getChildren().add( newItem);
        return newItem;
    }

    private void addChildren(TreeItem<FileResourceTreeItem> item, Path path, Integer fileId) {
        for(Iterator<Path> pi = path.iterator(); pi.hasNext();) {
            Path p = pi.next();
            item = addChild(p, item, fileId);
        }
    }

    private TreeItem<FileResourceTreeItem> addChild(Path p, TreeItem<FileResourceTreeItem> item, Integer fileId) {
        for( TreeItem<FileResourceTreeItem> ic: item.getChildren()) {
            if( ic.getValue().getDisplayName().equals(p.getFileName().toString())) {
                return ic;
            }
        }
        TreeItem<FileResourceTreeItem> newItem = new TreeItem<>(new FileResourceTreeItem(p.getFileName().toString(), fileId));
        item.getChildren().add( newItem);
        return newItem;
    }

    public TreeItem<FileResourceTreeItem> getRootItem() {
        return rootItem;
    }
}

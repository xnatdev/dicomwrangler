package org.nrg.db.ui;


/**
 * Created by davidmaffitt on 11/29/16.
 */
public abstract class TreeItemRule <C> {
    protected C context;
    protected String value;

    /**
     * Sets the context for this rule and evaluates its value in that context.
     *
     * @param context
     * @return this instance to allow chaining of methods.
     */
    public TreeItemRule<C> setContext( C context) {
        this.context = context;
        value = evaluate();
        return this;
    }

    /**
     * Evaluates the String value of this rule in context.
     *
     * @return
     */
    protected abstract String evaluate();

    /**
     * Test if the value of this rule in it's context matches the supplied string value.
     *
     * Usage is item.setContext( C).matches( String);
     *
     * @param value
     * @return true if
     */
    public boolean matches( String value) {
        return (this.value != null)? this.value.equals(value): false;
    }

    /**
     * The value this rule evaluates too in it's current context.
     *
     * @return String value
     */
    public String getValue() {
        return value;
    }
}

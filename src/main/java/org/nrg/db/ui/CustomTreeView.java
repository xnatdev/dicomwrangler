package org.nrg.db.ui;

import javafx.scene.control.TreeItem;
import org.nrg.db.model.FileResource;

import java.nio.file.Path;
import java.util.Iterator;
import java.util.List;

/**
 * Created by drm on 9/23/16.
 */
public class CustomTreeView {
    private TreeItem<FileResourceTreeItem> rootItem;
    private List<TreeItemRule> rules;

    public CustomTreeView(List<FileResource> resources, List<TreeItemRule> rules) {
        rootItem = new TreeItem<>(new FileResourceTreeItem("DICOM Files", null));
        rootItem.setExpanded(true);

        this.rules = rules;

        for(FileResource resource: resources) {
            TreeItem<FileResourceTreeItem> item = rootItem;
            for( TreeItemRule rule: rules) {
                item = addItem( item, rule, resource);
            }
        }
    }

    private TreeItem<FileResourceTreeItem> addItem(TreeItem<FileResourceTreeItem> item, TreeItemRule rule, FileResource resource) {
        for( TreeItem<FileResourceTreeItem> childItem: item.getChildren()) {
            if( rule.setContext(resource.getId()).matches(childItem.getValue().getDisplayName())) {
                return childItem;
            }
        }
        return addChild( item, rule, resource);
    }

    private TreeItem<FileResourceTreeItem> addChild( TreeItem<FileResourceTreeItem> item, TreeItemRule rule, FileResource resource) {
        rule.setContext( resource.getId());
        TreeItem<FileResourceTreeItem> newItem = new TreeItem<>(new FileResourceTreeItem(rule.getValue(), resource.getId()));
        item.getChildren().add( newItem);
        return newItem;
    }

    public TreeItem<FileResourceTreeItem> getRootItem() {
        return rootItem;
    }
}

package org.nrg.db;

import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
//import net.java.truevfs.access.TPath;
import javafx.stage.FileChooser;
import org.nrg.db.model.FileResource;
import org.nrg.db.persistence.DicomBrowserRepository;
import org.nrg.db.ui.DicomElementI;
import org.nrg.db.ui.DicomElementView;
import org.nrg.db.ui.FileResourceTreeItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.event.EventListener;

import javax.swing.JFileChooser;
import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;


public class MainScreenController implements Initializable, ApplicationEventPublisherAware {
    @FXML
    private TreeView<FileResourceTreeItem> fileTreeView;
    @FXML
    private AnchorPane ap;

    @FXML
    private TreeTableView<DicomElementI> treeTableView;

    private DirectoryChooser directoryChooser;
    private FileChooser fileChooser;
    private JFileChooser chooser;
    private ApplicationEventPublisher publisher;
    @Autowired
    private DicomBrowserRepository dbr;
    @Autowired DicomElementView dev;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.fileChooser = new FileChooser();
        this.directoryChooser = new DirectoryChooser();
        this.chooser = new JFileChooser(".");
        this.fileChooser.setTitle("Select file or directory to scan.");

        List<FileResource> fileResources = dbr.getResources();
        fileTreeView.setRoot( TreeViewFactory.createDefaultTreeViewRoot( fileResources));

        fileTreeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        fileTreeView.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<TreeItem<FileResourceTreeItem>>() {
            @Override
            public void onChanged(Change<? extends TreeItem<FileResourceTreeItem>> c) {
                System.out.println(" You selected: " + c);
                List<Integer> selectedFileIds = getSelectedFileIds( fileTreeView.getSelectionModel().getSelectedItems());

                for( Integer fileId: selectedFileIds) {
                    System.out.println( "File ID: " + fileId);
                }
                publisher.publishEvent( new ResourcesSelectedEvent( this, selectedFileIds));
            }
        });
    }

    private List<Integer> getSelectedFileIds( List<TreeItem<FileResourceTreeItem>> selectedItems) {
        List<Integer> selectedFileIds = new ArrayList<>();
        for( TreeItem<FileResourceTreeItem> item: selectedItems) {
            selectedFileIds.addAll( getSelectedFileIds(item));
        }
        return selectedFileIds;
    }

    private List<Integer> getSelectedFileIds( TreeItem<FileResourceTreeItem> selectedItem) {
        List<Integer> selectedFileIds = new ArrayList<>();
        List<TreeItem<FileResourceTreeItem>> children = selectedItem.getChildren();
        if( children.isEmpty()) {
            selectedFileIds.add( selectedItem.getValue().getFileId());
        }
        else {
            selectedFileIds.addAll( getSelectedFileIds( children));
        }
        return selectedFileIds;
    }

    public Node getRoot() {
        return ap;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

//    @FXML
//    public void handleOpenMenuAction(ActionEvent event) {
//        System.out.println("Open menu item action.");
//        chooser.setMultiSelectionEnabled(true);
//        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
//        int ret = chooser.showOpenDialog(null);
//        if(ret == JFileChooser.APPROVE_OPTION) {
//            File[] files = chooser.getSelectedFiles();
//            List<Path> rootPaths = new ArrayList<>(files.length);
//            for( File f: files) {
////                rootPaths.add( new TPath(f));
//                rootPaths.add( f.toPath());
//            }
//            publisher.publishEvent( new SourcePathsProvidedEvent( this, rootPaths));
//        }
//    }

    @FXML
    public void handleOpenFilesMenuAction(ActionEvent event) {
        System.out.println("Open menu item action.");
        List<File> files = fileChooser.showOpenMultipleDialog( ap.getScene().getWindow());
        List<Path> rootPaths = new ArrayList<>();
        if( files != null) {
            for( File f: files) {
//                rootPaths.add( new TPath(f));
                rootPaths.add( f.toPath());
            }
        }
        publisher.publishEvent( new SourcePathsProvidedEvent( this, rootPaths));
    }

    @FXML
    public void handleOpenDirectoryMenuAction(ActionEvent event) {
        System.out.println("Open directory menu item action.");
        File dir = directoryChooser.showDialog( ap.getScene().getWindow());
        List<Path> rootPaths = new ArrayList<>();
        if( dir != null) {
//            rootPaths.add( new TPath(f));
            rootPaths.add( dir.toPath());
        }
        publisher.publishEvent( new SourcePathsProvidedEvent( this, rootPaths));
    }

    @FXML
    public void handleDumpElementsMenuAction(ActionEvent event) {
        System.out.println("Dump elements menu item action.");

        DicomElementView.dumpElements( treeTableView.getRoot().getChildren(), 0);
    }

    @FXML
    public void handleClearMenuAction( ActionEvent event) {
        System.out.println("Clear menu item action.");
        dbr.clear();

        treeTableView.setRoot( new DicomElementView().getRootItem());
        fileTreeView.setRoot( TreeViewFactory.createDefaultTreeViewRoot( new ArrayList<>()));
    }

    @FXML
    public void handleCloseMenuAction( ActionEvent event) {
        System.out.println("Close menu item action.");
        dbr.clear();

        Platform.exit();
    }

    @EventListener
    public void handleResourcesOpenedEvent(ResourcesOpenedEvent event) {
        System.out.println("Adding files to tree view.");
        List<FileResource> resources = dbr.getResources();
        fileTreeView.setRoot( TreeViewFactory.createDefaultTreeViewRoot( resources));
    }

    @EventListener
    public void handleResourcesSelectedEvent(ResourcesSelectedEvent event) {
        dev.setDicomElements( dbr.getElements( event.getSelectedFileIds()));
        treeTableView.setShowRoot(false);
        treeTableView.setRoot( dev.getRootItem());
        treeTableView.getColumns().setAll(dev.getColumns());
        treeTableView.getSelectionModel().setCellSelectionEnabled(true);
    }

    @EventListener
    public void handleChangeElementViewOrderEvent(ChangeElementViewOrderEvent event) {
        System.out.println("Got a changeElemntViewEvent: " + event);
        TreeItem<DicomElementI> selectedItem = event.getItem();

        // get the selectedItem's children and cut them loose.
        List<TreeItem<DicomElementI>> kids = selectedItem.getChildren().stream().collect(Collectors.toList());
        selectedItem.getChildren().clear();

        // get all the grandkids.
        List<TreeItem<DicomElementI>> grandkids = new ArrayList<TreeItem<DicomElementI>>(); ;
        for( TreeItem<DicomElementI> kid: kids) {
            grandkids.addAll(kid.getChildren());
        }

        Map<String, TreeItem<DicomElementI>> newKidMap = new HashMap<>();
        for( TreeItem<DicomElementI> gkid: grandkids) {
            String name = gkid.getValue().getName();
            int tag = gkid.getValue().getTag();
            if( ! newKidMap.containsKey(name)) {
                DicomElementI de = new DicomElementI(tag, name, "", 0);
                TreeItem<DicomElementI> newKid = new TreeItem<>(de);
                selectedItem.getChildren().add(newKid);
                newKidMap.put(name, newKid);
            }
            TreeItem<DicomElementI> newkid = newKidMap.get(name);
            String gkidNewName = gkid.getParent().getValue().getTagString();
            gkid.getValue().setName( gkidNewName);
            newkid.getChildren().add(gkid);
        }
    }

}

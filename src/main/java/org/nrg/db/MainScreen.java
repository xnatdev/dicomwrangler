package org.nrg.db;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;

/**
 * Created by drm on 7/3/16.
 */
public class MainScreen extends StackPane {

    public MainScreen( final MainScreenController msc, URL fxml) {
        FXMLLoader loader = new FXMLLoader(fxml);
        try {
            loader.setControllerFactory(new Callback<Class<?>, Object>() {

                @Override
                public Object call(Class<?> aClass) {
                    return msc;
                }
            });
            AnchorPane ap = loader.load();
            getChildren().addAll(ap);
        } catch (IOException e) {
            throw new RuntimeException( e);
        }
    }
}

package org.nrg.db;

import org.springframework.context.ApplicationEvent;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Created by drm on 7/4/16.
 */
public class ResourcesOpenedEvent extends ApplicationEvent {
    Map<Path, List<Path>> paths;

    public ResourcesOpenedEvent(Object source, Map<Path, List<Path>> paths) {
        super(source);
        this.paths = paths;
    }

    public Map<Path, List<Path>> getPaths() {
        return paths;
    }
}

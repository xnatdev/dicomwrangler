package org.nrg.db;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import sun.security.krb5.internal.PAData;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/**
 * Created by drm on 7/4/16.
 */
@Component
public class HandleSourcePathsProvidedEvent implements ApplicationEventPublisherAware{

    private ApplicationEventPublisher publisher;


    @EventListener
    public void handleAddSourceFiles( SourcePathsProvidedEvent event) {

        System.out.println("Searching source files for DICOM objects.");
        try {
            Map<Path, List<Path>> map = new HashMap<>();
            for( Path basePath: event.getPaths()) {
                if( basePath.toFile().isFile()) {
                    map.put(basePath.getParent(), Arrays.asList(basePath.getFileName()) );
                }
                else {
                    map.put( basePath, findResources(basePath, basePath));
                }
            }
            publisher.publishEvent(new ObjectPathsProvidedEvent(this, map));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Path> findResources(Path basePath, Path path) throws IOException {
        final List<Path>  filteredResources = new ArrayList<>();

        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if( file.toString().endsWith("dcm")) {
                        Path f = basePath.relativize(file);
                        filteredResources.add(f);
                    }
//                filteredResources.add(basePath.relativize(file));
                return FileVisitResult.CONTINUE;
            }
        });

        return filteredResources;
    }

    public List<Path> findResources(List<Path> paths) throws IOException {
        final List<Path>  filteredResources = new ArrayList<>();

        for( Path p: paths) {
            Files.walkFileTree(p, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if( file.endsWith("dcm")) {
                        filteredResources.add(file);
                    }
//                    filteredResources.add(file);
                    return FileVisitResult.CONTINUE;
                }
            });
        }
        return filteredResources;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

}

package org.nrg.db;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.nrg.db.ui.DicomElementView;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * Created by drm on 7/3/16.
 */
@Configuration
public class ScreensConfiguration {
    private Stage primaryStage;

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("DICOM Wrangler");
    }

    public void showScreen(Parent screen) {
        Scene scene = new Scene(screen, 1200, 600);
        scene.getStylesheets().add("dicomWrangler.css");
        primaryStage.setScene( scene);
        primaryStage.show();
    }

    public void showMainScreen() {
        showScreen( mainScreen());
    }

    @Bean
    public MainScreen mainScreen() {
        return new MainScreen( mainScreenController(), getClass().getResource("MainScreen.fxml"));
    }

    @Bean
    public MainScreenController mainScreenController() {
        return new MainScreenController();
    }

    @Bean
    public DicomElementView dicomElementView() {
        return new DicomElementView();
    }

}

create table files (
  id  SERIAL PRIMARY KEY,
  basepath varchar(1024),
  relativepath VARCHAR(1024),
  hash varchar(256),
  UNIQUE( basepath, relativepath)
);

create table attributes (
  id SERIAL PRIMARY KEY,
  tagpath varchar(256),
  name varchar(256),
  value varchar(1024),
  UNIQUE( tagpath, value)
);

create table files_attributes (
  id SERIAL PRIMARY KEY,
  file_id INTEGER REFERENCES files(id),
  attribute_id INTEGER REFERENCES attributes(id)
);

create index attributes_index on attributes (tagpath, value);